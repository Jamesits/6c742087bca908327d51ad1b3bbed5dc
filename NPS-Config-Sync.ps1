###Network Policy Server Synchronization Script
#This script copies the configuration from the NPS Master Server and imports it on the secondary server.
#The Account that this script runs under must have Local Administrator rights to the NPS Master.
#This was designed to be run as a scheduled task on the NPS Secondary Servers on an hourly,daily, or as-needed basis.
# Modified from https://deployhappiness.com/two-network-policy-server-tricks-subnets-and-syncing/
 
###Variables
#NPSMaster - Your Primary Network Policy Server you want to copy the config from.
$NPSMaster = "adds0.corp.contoso.com"
$NPSSecondary = "adds1.corp.contoso.com"
#NPSConfigTempFile - A temporary location to store the XML config. Make sure master, secondary and the computer you run this script all have this directory.
$NPSConfigTempFile = "C:\Windows\Temp\NPSConfig-$NPSMaster.xml"
 
#Create an NPS Sync Event Source if it doesn't already exist
if (!(get-eventlog -logname "System" -source "NPS-Sync")) {new-eventlog -logname "System" -source "NPS-Sync"}
 
#Write an error and exit the script if an exception is ever thrown
trap {write-eventlog -logname "System" -eventID 1 -source "NPS-Sync" -EntryType "Error" -Message "An Error occured during NPS Sync: $_. Script run from $($MyInvocation.MyCommand.Definition)"; exit}

$SourceSession = New-PSSession -ComputerName $NPSMaster
$DestSession = New-PSSession -ComputerName $NPSSecondary

#Connect to NPS Master and export configuration
Write-Host "Exporting config from master"
$configExportResult = invoke-command -Session $SourceSession -ArgumentList $NPSConfigTempFile -scriptBlock {param ($NPSConfigTempFile) netsh nps export filename = $NPSConfigTempFile exportPSK = yes}
Copy-Item -FromSession $SourceSession -Path $NPSConfigTempFile -Destination $NPSConfigTempFile
Invoke-Command -Session $SourceSession -ArgumentList $NPSConfigTempFile -scriptBlock {param ($NPSConfigTempFile) Remove-Item $NPSConfigTempFile}

#Verify that the import XML file was created. If it is not there, it will throw an exception caught by the trap above that will exit the script.
Write-Host "Verifying config"
$NPSConfigTest = Get-Item $NPSConfigTempFile

#Clear existing configuration and import new NPS config
Write-Host "Importing config to secondary"
Copy-Item -ToSession $DestSession -Path $NPSConfigTempFile -Destination $NPSConfigTempFile
$configClearResult = Invoke-Command -Session $DestSession -ArgumentList $NPSConfigTempFile -scriptBlock {netsh nps reset config}
$configImportResult = Invoke-Command -Session $DestSession -ArgumentList $NPSConfigTempFile -scriptBlock {param ($NPSConfigTempFile) netsh nps import filename = $NPSConfigTempFile}
Invoke-Command -Session $DestSession -ArgumentList $NPSConfigTempFile -scriptBlock {param ($NPSConfigTempFile) Remove-Item $NPSConfigTempFile}

#Delete Temporary File
Write-Host "Cleaning up"
Remove-Item -path $NPSConfigTempFile
 
#Compose and Write Success Event
$successText = "Network Policy Server Configuration successfully synchronized from $NPSMaster to $NPSSecondary.
 
Export Results: $configExportResult
 
Import Results: $configImportResult
 
Script was run from $($MyInvocation.MyCommand.Definition)"
 
write-eventlog -logname "System" -eventID 1 -source "NPS-Sync" -EntryType "Information" -Message $successText
Write-Host $successText